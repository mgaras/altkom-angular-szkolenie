ng g m playlists -m app
ng g c playlists/views/playlists-view --export true
 
ng g c playlists/components/items-list
ng g c playlists/components/list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form


ng g m music --routing true -m app
ng g c music/views/music-search-view --export true
ng g c music/components/search-form
ng g c music/components/search-results
ng g c music/components/album-card

ng g s music/services/music-search