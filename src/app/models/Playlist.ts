
// export class Playlist{ }

export interface Playlist {
    id: number
    name: string
    favorite: boolean
    /**
     * HEX value
     */
    color: string
    tracks?: Track[]
}

export interface Track { }
