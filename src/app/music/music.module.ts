import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicRoutingModule } from './music-routing.module';
import { MusicSearchViewComponent } from './views/music-search-view/music-search-view.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { environment } from 'src/environments/environment';
import { API_URL_TOKEN } from "./services/tokens";
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MusicSearchService } from './services/music-search.service';

@NgModule({
  declarations: [MusicSearchViewComponent, SearchFormComponent, SearchResultsComponent, AlbumCardComponent],
  imports: [
    CommonModule,
    MusicRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [MusicSearchViewComponent],
  providers: [
    MusicSearchService,
    {
      provide: API_URL_TOKEN,
      useValue: environment.api_url
    },
    // {
    //   provide: MusicSearchService,
    //   useFactory(url) {
    //     return new MusicSearchService(url)
    //   },
    //   deps: [
    //     API_URL_TOKEN
    //   ]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpotifyMusicSearchService,
    //   // deps: [
    //   //   TEST_API_URL_TOKEN
    //   // ]
    // },
    // {
    //   provide:MusicSearchService,
    //   useClass:MusicSearchService
    // },
    // MusicSearchService
  ]
})
export class MusicModule { }
