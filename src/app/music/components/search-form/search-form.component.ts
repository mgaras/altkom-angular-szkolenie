import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { filter, distinctUntilChanged, debounceTime } from 'rxjs/operators';

export const censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const hasError = 'string' == typeof control.value && control.value.includes('batman')

  return hasError ? {
    censor: { badword: 'batman' }
  } : null;
}

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @ViewChild('queryRef', { static: false })
  queryRef: ElementRef<HTMLInputElement>

  @Input()
  set query(q) {
    this.queryForm.get('query').setValue(q, {
      emitEvent: false
    })
  }

  // ngOnChanges(changes){
  //   changes.query
  // }

  queryForm = new FormGroup({
    query: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      censor
    ]),
  })

  constructor() {
    (window as any).form = this.queryForm

    this.queryForm.get('query').valueChanges
      .pipe(
        debounceTime(400),
        filter(q => q.length >= 3),
        distinctUntilChanged(),
      )
      .subscribe(query => {
        this.search(query)
      })
  }

  @Output()
  queryChange = new EventEmitter<string>();

  search(query: string) {
    this.queryChange.emit(query)
  }


  // // ngAfterContetntInit() {
  // ngAfterViewInit() {
  //   this.queryRef.nativeElement.focus()
  //   // console.log(this.queryRef)
  // }

  ngOnInit() {
  }

}
