import { Component, OnInit, Inject } from '@angular/core';
import { Album } from 'src/app/models/Album';
import { MusicSearchService } from '../../services/music-search.service';
import { Subscription, Subject, ConnectableObservable } from 'rxjs';
import { takeUntil, tap, multicast, refCount, share, shareReplay } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-music-search-view',
  templateUrl: './music-search-view.component.html',
  styleUrls: ['./music-search-view.component.scss']
})
export class MusicSearchViewComponent implements OnInit {

  query$ = this.musicSearch.query$


  albums$ = this.musicSearch.getAlbums().pipe(
    share()
  )

  message

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private musicSearch: MusicSearchService) {

      
    this.route.queryParamMap.subscribe(queryParamMap => {
      const q = queryParamMap.get('q')
      if (q) {
        this.search(q)
      }
    })

    this.route.queryParamMap.subscribe(queryParamMap => {
      const q = queryParamMap.get('q')
      if (q) {
        this.search(q)
      }
    })
  }

  search(query: string) {
    // this.router.navigate(['/music','albums',123,'comments',123])
    this.router.navigate([/* '/music' */], {
      queryParams: {
        q: query
      },
      relativeTo:this.route,
      replaceUrl:true
    })

    this.musicSearch.search(query)
  }

  ngOnInit() {

  }

}
